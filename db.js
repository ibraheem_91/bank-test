import mysql from "mysql2/promise";

let dbConnection;
const getDatabaseConnection = async () => {
    if (!dbConnection) {
        dbConnection = await mysql.createConnection({
            host: process.env.BANK_DB_HOST,
            user: process.env.BANK_DB_USER_NAME,
            database: process.env.BANK_DB_NAME
        });
    }
    return dbConnection;
};


export {getDatabaseConnection};