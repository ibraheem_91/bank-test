import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import _ from "lodash";
import {validateParams as validateTransferRequest, processTransfer, logTransactions} from "./bank-services/transafer";


const app = express();

//Any client can call it, you will receive requests from the bank's mobile app
app.use(cors());

//for preflight
app.options('*', cors());

//To parse body payload
app.use(bodyParser.json({
    limit: '100kb', // config.get('BODY_LIMIT')
}));

//ensures it comes from a legit source
const whitelist = ['*'];
const corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
};


let RateLimit = require('express-rate-limit');

//allow only one request to transfer per user
let apiLimiter = new RateLimit({
    windowMs: 1 * 60 * 1000, // 15 minutes
    max: 1,
    delayMs: 0,// disabled
    message: 'You are allowed to transfer once per minute',
});

// only apply to requests that begin with /api/
app.use('/transfer', apiLimiter);


/**
 * Transfer from account to account
 * response = {
 *
 *      status:"Success|Error",
 *      data:{},
 *      error:{
 *      code:"404",
 *      msg:"not found"
 *      }
 *
 * }
 */
app.post('/transfer', /*cors(corsOptions),*/async function (req, res, next) {

    //1-Validate Request data
    let validatedRequestData = await  validateTransferRequest(req);
    let msg = _.get(validatedRequestData, 'msg', '');
    let code = _.get(validatedRequestData, 'code', '');

    if (!_.isEmpty(msg) && !_.isEmpty(code)) {
        return res.json({
            status: "Error",
            data: {},
            error: {
                code: code,
                msg: msg,
            }
        });
    }
    //2-Process Transfer
    let result = await processTransfer(validatedRequestData);

    //3-log transactions , but don't wait for it,ASYNC log
    logTransactions(validatedRequestData).then((rs) => {
        //console.log("Transactions have been logged in DB");
    });

    return res.json(result);

});

app.listen(8989);
console.log('Listening on port 8989...');
