import _ from "lodash";
import randomID from "random-id";
import {getDatabaseConnection} from "../db";

const validateParams = async (req) => {

    let fromAccount = _.get(req, 'body.fromAccount', '').trim();
    let toAccount = _.get(req, 'body.toAccount', '', '').trim();
    let amount = _.get(req, 'body.amount', 0);

    //validate request data
    if (_.isEmpty(fromAccount) || _.isEmpty(toAccount) || _.isEmpty(amount) || amount === 0) {
        return {
            code: "400",
            msg: "`fromAccount`, `toAccount`, and `amount` are mandatory fields",
        };

    }
    //check the amount if float
    amount = parseFloat(amount);
    if (isNaN(amount)) {

        return {
            code: "400",
            msg: "`amount` is not a valid float\"",
        };

    }
    //zero amount to transfer?
    if (amount === 0) {
        return {
            code: "400",
            msg: "please add the amount you want to transfer",
        };

    }

    // 2)fromAccount !== toAccount
    if (fromAccount === toAccount) {
        return {
            code: "400",
            msg: "you can not transfer to the same account",
        };

    }

    let connection;
    let rows, fields;
    try {
        connection = await getDatabaseConnection();
    } catch (err) {
        return {
            code: "400",
            msg: "we are facing some difficulties please try again later",
        };

    }
    // 3-) toAccount & fromAccount exists in balances table
    try {
        [rows, fields] = await connection.execute('SELECT COUNT(id) as numberOfAccounts FROM `balances` WHERE `account` IN(?,?)', [fromAccount, toAccount]);
    } catch (err) {

        return {
            code: "400",
            msg: "we are facing some difficulties please try again later",
        };

    }
    if (_.get(rows, '0.numberOfAccounts', 0) !== 2) {

        return {
            code: "404",
            msg: "one of the accounts does not exist please verify them",
        };

    }

    //4-) make sure the amount we need to transfer from, is greater than or equals to the existing baalance
    try {
        [rows, fields] = await connection.execute('SELECT balance FROM `balances` WHERE `account`=?', [fromAccount]);
    } catch (err) {
        return {
            code: "400",
            msg: "we are facing some difficulties please try again later",
        };

    }
    let fromAccountBalance = _.get(rows, '0.balance', 0);
    if (amount > fromAccountBalance) {
        return {
            code: "400",
            msg: "insufficient balance",
        };
    }

    return {
        fromAccount,
        toAccount,
        amount,
        fromAccountBalance,
    };
};


const processTransfer = async (validatedReqData) => {

    let fromAccount = _.get(validatedReqData, 'fromAccount', '');
    let toAccount = _.get(validatedReqData, 'toAccount', '', '');
    let amount = _.get(validatedReqData, 'amount', 0);
    let fromAccountBalance = _.get(validatedReqData, 'fromAccountBalance', 0);

    let connection;
    let rows, fields;

    try {
        connection = await getDatabaseConnection();

        //============ START THE TRANSFER LOGIC
        //Check the request if already in progress or done,
        // So we created a new table which log the action and update the status from progress to done
        [rows, fields] = await connection.execute('SELECT status FROM `action_log` WHERE `account`=? AND `action_name`=?', [fromAccount, 'transfer']);

    } catch (err) {

        return {
            status: "Error",
            data: {},
            error: {
                code: "400",
                msg: "we are facing some difficulties please try again later",
            }
        }
    }

    //Check the request if already in progress or done,
    let currentStatus = _.get(rows, '0.status', '');
    if (currentStatus === 'inProgress') {
        return {
            status: "inProgress",
            data: {},
            error: {}
        };
    }

    try {
        //Start the transaction
        await connection.beginTransaction();
        //add action log
        await connection.execute('INSERT INTO action_log(action_name,status,account) VALUES(?,?,?)', ['transfer', 'inProgress', fromAccount]);
        //update balance fromAccount
        await connection.execute('UPDATE `balances` SET `balance`=? WHERE `account`=? ', [fromAccountBalance - amount, fromAccount]);
        //update balance toAccount
        [rows, fields] = await connection.execute('SELECT balance from `balances` where `account`=? ', [toAccount]);
        let toAccountBalance = _.get(rows, '0.balance', 0);
        await connection.execute('UPDATE `balances` SET `balance`=? WHERE `account`=? ', [toAccountBalance + amount, toAccount]);
        //update the action_log to done
        await connection.execute('UPDATE `action_log` SET status="done" where action_name=? and account=?  ', ['transfer', fromAccount]);
        //Commit transaction
        await connection.commit();

    } catch (err) {
        //rollback
        await connection.rollback();
        return {
            status: "Error",
            data: {},
            error: {
                code: "400",
                msg: err.message,
            }
        };
    }

    return {
        status: "Success",
        data: {
            fromAccount,
            toAccount,
            amount,
            balance: fromAccountBalance - amount,
        },
        error: {}
    };

};

const logTransactions = async (validatedReqData) => {
    let fromAccount = _.get(validatedReqData, 'fromAccount', '');
    let toAccount = _.get(validatedReqData, 'toAccount', '', '');
    let amount = _.get(validatedReqData, 'amount', 0);

    let connection;

    try {
        connection = await getDatabaseConnection();

        //add transaction to both accounts
        connection.execute('INSERT INTO transactions(reference,account,amount) VALUES(?,?,?)', [randomID(20, "0"), fromAccount, -amount], (rows, fields) => {
            //may be here we can Log to DB
        });

        connection.execute('INSERT INTO transactions(reference,account,amount) VALUES(?,?,?)', [randomID(20, "0"), toAccount, +amount], (rows, fields) => {
            //may be here we can Log to DB
        });

    } catch (err) {
        //log Errors to file or to db
    }
};


export {validateParams, processTransfer, logTransactions}
